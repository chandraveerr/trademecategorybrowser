package com.trademeapplication.marketplace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.trademeapplication.marketplace.ui.CategoryListFragment;
import com.trademeapplication.marketplace.ui.ListDetailFragment;
import com.trademeapplication.marketplace.ui.ProductListFragment;
import com.trademeapplication.marketplace.utils.Utils;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.trademeapplication.marketplace", appContext.getPackageName());
    }

    @Test
    public void getDetailFragment_Test() throws Exception {
        assertNotEquals(Utils.getDetailFragment("123"), new ListDetailFragment());
    }

    @Test
    public void getCategoryListFragment_Test() throws Exception {
        assertNotEquals(Utils.getCategoryListFragment("123"), new CategoryListFragment());
    }

    @Test
    public void getProductListFragment_Test() throws Exception {
        assertNotEquals(Utils.getProductListFragment("123"), new ProductListFragment());
    }

    @Test
    public void header_isCorrect() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals(Utils.getAuthHeader(appContext), "OAuth oauth_consumer_key=\"A1AC63F0332A131A78FAC304D007E7D1\", oauth_signature=\"EC7F18B17A062962C6930A8AE88B16C7%26\", oauth_signature_method=\"PLAINTEXT\"");
    }

}
