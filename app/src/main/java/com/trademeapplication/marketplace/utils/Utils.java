package com.trademeapplication.marketplace.utils;

import android.content.Context;
import android.os.Bundle;

import com.trademeapplication.marketplace.BuildConfig;
import com.trademeapplication.marketplace.R;
import com.trademeapplication.marketplace.ui.CategoryListFragment;
import com.trademeapplication.marketplace.ui.ListDetailFragment;
import com.trademeapplication.marketplace.ui.ProductListFragment;

/**
 * Class to hold general utility method not to initialized
 */

public final class Utils {

    public interface ApiConstants {

        int    rowstofetch  = 20;

        int    defaultDepth = 1;

        String KEY_ID       = "key_id";
    }

    private Utils() {

    }

    public static String getAuthHeader(Context context) {
        return context.getString(R.string.oauth_request, BuildConfig.CONSUMER_KEY, BuildConfig.CONSUMER_SECRET, "PLAINTEXT");
    }

    /** Creates Listing Detail fragment for specific product ID */
    public static ListDetailFragment getDetailFragment(String id) {
        ListDetailFragment fragment = new ListDetailFragment();
        Bundle args = new Bundle();
        args.putString(ApiConstants.KEY_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    /** Creates CategoryListFragment for specific product ID */
    public static CategoryListFragment getCategoryListFragment(String categoryId) {
        CategoryListFragment fragment = new CategoryListFragment();
        Bundle args = new Bundle();
        args.putString(ApiConstants.KEY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    /** Creates ProductListFragment for specific product ID */
    public static ProductListFragment getProductListFragment(String categoryId) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(ApiConstants.KEY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }
}
