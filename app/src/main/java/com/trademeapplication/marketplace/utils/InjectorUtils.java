package com.trademeapplication.marketplace.utils;

import com.trademeapplication.marketplace.data.DataRepository;
import com.trademeapplication.marketplace.viewmodel.CategoryViewModelFactory;
import com.trademeapplication.marketplace.viewmodel.ListingDetailViewModelFactory;
import com.trademeapplication.marketplace.viewmodel.ListingViewModelFactory;

import android.support.annotation.NonNull;

/**
 * Provides static methods to inject the various classes needed for
 * CategoryListing
 */

public class InjectorUtils {

    public static DataRepository provideRepository() {
        return DataRepository.getInstance();
    }

    public static CategoryViewModelFactory provideCategoryViewModelFactory(@NonNull String id) {
        DataRepository repository = provideRepository();
        return new CategoryViewModelFactory(repository, id);
    }

    public static ListingDetailViewModelFactory provideListingDetailViewModelFactory(@NonNull String id) {
        DataRepository repository = provideRepository();
        return new ListingDetailViewModelFactory(repository, id);
    }

    public static ListingViewModelFactory provideListingViewModelFactory(@NonNull String id) {
        DataRepository repository = provideRepository();
        return new ListingViewModelFactory(repository, id);
    }
}
