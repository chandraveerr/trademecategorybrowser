package com.trademeapplication.marketplace.utils;

import com.trademeapplication.marketplace.R;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * A router class for screen switching
 */

public class NavigationUtils {

    public static void startScreen(@NonNull FragmentManager fragmentmanager, @NonNull Fragment fragment) {
        fragmentmanager.beginTransaction().addToBackStack("product").replace(R.id.fragment_container, fragment, null).commit();
    }

    public static void startScreenDetailMode(@NonNull FragmentManager fragmentmanager, @NonNull Fragment fragment) {
        fragmentmanager.beginTransaction().replace(R.id.item_detail_container, fragment, null).commit();
    }
}
