package com.trademeapplication.marketplace.utils;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;
import com.trademeapplication.marketplace.R;

import android.content.Context;

/**
 * Configuration class for glide
 */

@GlideModule
public class GlideConfiguration extends AppGlideModule {

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        // Apply options you wish to the Glide Builder
        builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_RGB_565).error(R.drawable.error_img_album).placeholder(R.drawable.error_img_album).centerCrop());
    }

}
