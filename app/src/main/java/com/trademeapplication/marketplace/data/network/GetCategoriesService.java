package com.trademeapplication.marketplace.data.network;

import com.trademeapplication.marketplace.data.model.Categories;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Makes a GET call to get Categories of a particular Listing.
 *
 * @param number
 *            Category id for subcategories.
 * @param depth
 *            levels to go down.
 * @return sub categories for the the give category
 */

public interface GetCategoriesService {

    @GET("v1/Categories/{number}.json")
    Call<Categories> getCategories(@Path("number") String number, @Query("depth") int depth);

    @GET("v1/Categories/{number}.json")
    Observable<Categories> getCategoriesRx(@Path("number") String number, @Query("depth") int depth);
}
