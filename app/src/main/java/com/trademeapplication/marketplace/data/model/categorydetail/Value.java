
package com.trademeapplication.marketplace.data.model.categorydetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("Thumbnail")
    @Expose
    private String  thumbnail;

    @SerializedName("List")
    @Expose
    private String  list;

    @SerializedName("Medium")
    @Expose
    private String  medium;

    @SerializedName("Gallery")
    @Expose
    private String  gallery;

    @SerializedName("Large")
    @Expose
    private String  large;

    @SerializedName("FullSize")
    @Expose
    private String  fullSize;

    @SerializedName("PhotoId")
    @Expose
    private Integer photoId;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getFullSize() {
        return fullSize;
    }

    public void setFullSize(String fullSize) {
        this.fullSize = fullSize;
    }

    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

}
