
package com.trademeapplication.marketplace.data.model.categorydetail;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdditionalData {

    @SerializedName("BulletPoints")
    @Expose
    private List<Object> bulletPoints = null;

    @SerializedName("Tags")
    @Expose
    private List<Object> tags         = null;

    public List<Object> getBulletPoints() {
        return bulletPoints;
    }

    public void setBulletPoints(List<Object> bulletPoints) {
        this.bulletPoints = bulletPoints;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

}
