
package com.trademeapplication.marketplace.data.model.categorydetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Questions {

    @SerializedName("TotalCount")
    @Expose
    private Integer                                                                           totalCount;

    @SerializedName("Page")
    @Expose
    private Integer                                                                           page;

    @SerializedName("PageSize")
    @Expose
    private Integer                                                                           pageSize;

    @SerializedName("List")
    @Expose
    private java.util.List<com.trademeapplication.marketplace.data.model.categorydetail.List> list = null;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public java.util.List<com.trademeapplication.marketplace.data.model.categorydetail.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.trademeapplication.marketplace.data.model.categorydetail.List> list) {
        this.list = list;
    }

}
