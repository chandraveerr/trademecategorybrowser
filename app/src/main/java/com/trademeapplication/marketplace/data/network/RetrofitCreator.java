package com.trademeapplication.marketplace.data.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.trademeapplication.marketplace.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitCreator {

    private static Retrofit       retrofit;

    static HttpLoggingInterceptor logging  = new HttpLoggingInterceptor();

    private static final String   BASE_URL = "https://api.tmsandbox.co.nz/";

    public static Retrofit getRetrofitInstance() {

        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder().baseUrl(BASE_URL).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).client(getHttpClient().build()).build();
        }
        return retrofit;
    }

    private static OkHttpClient.Builder getHttpClient() {
        // set your desired log level base on build flavour
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        else
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add logging as interceptor
        httpClient.addInterceptor(logging);
        return httpClient;
    }
}
