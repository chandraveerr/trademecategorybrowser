
package com.trademeapplication.marketplace.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Listings {

    @SerializedName("TotalCount")
    @Expose
    private Integer                 totalCount;

    @SerializedName("Page")
    @Expose
    private Integer                 page;

    @SerializedName("PageSize")
    @Expose
    private Integer                 pageSize;

    @SerializedName("List")
    @Expose
    private java.util.List<Listing> listing         = null;

    @SerializedName("DidYouMean")
    @Expose
    private String                  didYouMean;

    @SerializedName("FoundCategories")
    @Expose
    private java.util.List<Object>  foundCategories = null;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public java.util.List<Listing> getList() {
        return listing;
    }

    public void setList(java.util.List<Listing> listing) {
        this.listing = listing;
    }

    public String getDidYouMean() {
        return didYouMean;
    }

    public void setDidYouMean(String didYouMean) {
        this.didYouMean = didYouMean;
    }

    public java.util.List<Object> getFoundCategories() {
        return foundCategories;
    }

    public void setFoundCategories(java.util.List<Object> foundCategories) {
        this.foundCategories = foundCategories;
    }

}
