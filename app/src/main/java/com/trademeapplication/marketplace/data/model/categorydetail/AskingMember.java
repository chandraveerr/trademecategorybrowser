
package com.trademeapplication.marketplace.data.model.categorydetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AskingMember {

    @SerializedName("MemberId")
    @Expose
    private Integer memberId;

    @SerializedName("Nickname")
    @Expose
    private String  nickname;

    @SerializedName("DateAddressVerified")
    @Expose
    private String  dateAddressVerified;

    @SerializedName("DateJoined")
    @Expose
    private String  dateJoined;

    @SerializedName("UniqueNegative")
    @Expose
    private Integer uniqueNegative;

    @SerializedName("UniquePositive")
    @Expose
    private Integer uniquePositive;

    @SerializedName("FeedbackCount")
    @Expose
    private Integer feedbackCount;

    @SerializedName("IsAddressVerified")
    @Expose
    private Boolean isAddressVerified;

    @SerializedName("IsAuthenticated")
    @Expose
    private Boolean isAuthenticated;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDateAddressVerified() {
        return dateAddressVerified;
    }

    public void setDateAddressVerified(String dateAddressVerified) {
        this.dateAddressVerified = dateAddressVerified;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Integer getUniqueNegative() {
        return uniqueNegative;
    }

    public void setUniqueNegative(Integer uniqueNegative) {
        this.uniqueNegative = uniqueNegative;
    }

    public Integer getUniquePositive() {
        return uniquePositive;
    }

    public void setUniquePositive(Integer uniquePositive) {
        this.uniquePositive = uniquePositive;
    }

    public Integer getFeedbackCount() {
        return feedbackCount;
    }

    public void setFeedbackCount(Integer feedbackCount) {
        this.feedbackCount = feedbackCount;
    }

    public Boolean getIsAddressVerified() {
        return isAddressVerified;
    }

    public void setIsAddressVerified(Boolean isAddressVerified) {
        this.isAddressVerified = isAddressVerified;
    }

    public Boolean getIsAuthenticated() {
        return isAuthenticated;
    }

    public void setIsAuthenticated(Boolean isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }

}
