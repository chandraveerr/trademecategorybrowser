
package com.trademeapplication.marketplace.data.model.categorydetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("ListingId")
    @Expose
    private Integer      listingId;

    @SerializedName("ListingQuestionId")
    @Expose
    private Integer      listingQuestionId;

    @SerializedName("Comment")
    @Expose
    private String       comment;

    @SerializedName("CommentDate")
    @Expose
    private String       commentDate;

    @SerializedName("IsSellerComment")
    @Expose
    private Boolean      isSellerComment;

    @SerializedName("Answer")
    @Expose
    private String       answer;

    @SerializedName("AnswerDate")
    @Expose
    private String       answerDate;

    @SerializedName("AskingMember")
    @Expose
    private AskingMember askingMember;

    public Integer getListingId() {
        return listingId;
    }

    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    public Integer getListingQuestionId() {
        return listingQuestionId;
    }

    public void setListingQuestionId(Integer listingQuestionId) {
        this.listingQuestionId = listingQuestionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public Boolean getIsSellerComment() {
        return isSellerComment;
    }

    public void setIsSellerComment(Boolean isSellerComment) {
        this.isSellerComment = isSellerComment;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerDate() {
        return answerDate;
    }

    public void setAnswerDate(String answerDate) {
        this.answerDate = answerDate;
    }

    public AskingMember getAskingMember() {
        return askingMember;
    }

    public void setAskingMember(AskingMember askingMember) {
        this.askingMember = askingMember;
    }

}
