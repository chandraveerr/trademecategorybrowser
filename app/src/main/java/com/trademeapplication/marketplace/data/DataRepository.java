package com.trademeapplication.marketplace.data;

import com.trademeapplication.marketplace.data.model.Categories;
import com.trademeapplication.marketplace.data.model.Listings;
import com.trademeapplication.marketplace.data.model.categorydetail.CategoryDetail;
import com.trademeapplication.marketplace.data.network.CategoryNetworkDataSource;
import com.trademeapplication.marketplace.data.network.ListDetailNetworkDataSource;
import com.trademeapplication.marketplace.data.network.ListingsNetworkDataSource;
import com.trademeapplication.marketplace.data.network.Resource;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * A generic data repository class which interacts with different data sources
 * and provide data to intracting view models
 */

public class DataRepository {

    private static String         LOG_TAG = DataRepository.class.getSimpleName();

    private static DataRepository sInstance;

    // For Singleton instantiation
    private static final Object   LOCK    = new Object();

    public LiveData<Resource<Categories>> getCategories(@NonNull String id) {

        return CategoryNetworkDataSource.getInstance().getCategories(id);
    }

    public LiveData<Resource<Listings>> getListings(@NonNull String id) {
        return ListingsNetworkDataSource.getInstance().getProductList(id);
    }

    public synchronized static DataRepository getInstance() {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new DataRepository();
                Log.d(LOG_TAG, "Made new repository");
            }
        }
        return sInstance;
    }

    public LiveData<Resource<CategoryDetail>> getListingDetails(@NonNull String id) {
        return ListDetailNetworkDataSource.getInstance().getListingDetail(id);
    }
}
