package com.trademeapplication.marketplace.data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.trademeapplication.marketplace.data.model.Categories;
import com.trademeapplication.marketplace.utils.Utils;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Network source fetching category
 */

public class CategoryNetworkDataSource {

    private static CategoryNetworkDataSource      sInstance;

    private static String                         LOG_TAG = CategoryNetworkDataSource.class.getSimpleName();

    // For Singleton instantiation
    private static final Object                   LOCK    = new Object();

    private MutableLiveData<Resource<Categories>> mCategories;

    private GetCategoriesService                  service;

    private CategoryNetworkDataSource() {
        // mCategories = new MutableLiveData<>();
        service = RetrofitCreator.getRetrofitInstance().create(GetCategoriesService.class);
    }

    /**
     * Get the singleton for this class
     */
    public static CategoryNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new CategoryNetworkDataSource();
                Log.d(LOG_TAG, "Made new network data source");
            }
        }
        return sInstance;
    }

    public LiveData<Resource<Categories>> getCategories(String number) {
        mCategories = new MutableLiveData<>();
        mCategories.postValue(Resource.<Categories> loading(null));
        fetchCategories(number, Utils.ApiConstants.defaultDepth);
        return mCategories;
    }

    private void fetchCategories(String number, int depth) {

        service.getCategoriesRx(number, depth).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Categories>() {

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Categories value) {
                mCategories.postValue(Resource.success(value));
            }

            @Override
            public void onError(Throwable e) {
                Log.e(LOG_TAG, e.getMessage());
                mCategories.postValue(Resource.<Categories> error(e.getMessage(), null));
            }

            @Override
            public void onComplete() {

            }
        });
    }

}
