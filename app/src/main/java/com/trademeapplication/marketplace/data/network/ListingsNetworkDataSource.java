package com.trademeapplication.marketplace.data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.trademeapplication.marketplace.TradeMeApplication;
import com.trademeapplication.marketplace.data.model.Listings;
import com.trademeapplication.marketplace.utils.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Network source fetching listing
 */

public class ListingsNetworkDataSource {

    private static ListingsNetworkDataSource    sInstance;

    private static String                       LOG_TAG = ListingsNetworkDataSource.class.getSimpleName();

    // For Singleton instantiation
    private static final Object                 LOCK    = new Object();

    private MutableLiveData<Resource<Listings>> mListings;

    private GetProductListService               service;

    private ListingsNetworkDataSource() {
        service = RetrofitCreator.getRetrofitInstance().create(GetProductListService.class);
    }

    /**
     * Get the singleton for this class
     */
    public static ListingsNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new ListingsNetworkDataSource();
                Log.d(LOG_TAG, "Made new network data source");
            }
        }
        return sInstance;
    }

    public LiveData<Resource<Listings>> getProductList(String number) {
        mListings = new MutableLiveData<>();
        mListings.postValue(Resource.<Listings> loading(null));
        fetchProductList(number);
        return mListings;
    }

    private void fetchProductList(String category) {

        service.getProductListRx(Utils.getAuthHeader(TradeMeApplication.getContext()), category, Utils.ApiConstants.rowstofetch).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DefaultObserver<Listings>() {

            @Override
            public void onNext(Listings value) {
                mListings.postValue(Resource.success(value));
            }

            @Override
            public void onError(Throwable t) {
                Log.e(LOG_TAG, t.getMessage());
                mListings.postValue(Resource.<Listings> error(t.getMessage(), null));
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
