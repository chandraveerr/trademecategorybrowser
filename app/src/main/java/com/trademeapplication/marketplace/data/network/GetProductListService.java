package com.trademeapplication.marketplace.data.network;

import com.trademeapplication.marketplace.data.model.Listings;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Makes a GET call to get Details of a particular Listing.
 * 
 * @param category
 *            Category id of a listings in that category.
 * @param authorization
 *            Specifies the OAUTH 1.0 authorization header.
 * @param rows
 *            numbe rof listings.
 * @return Listings available.
 */

public interface GetProductListService {

    @GET("v1/Search/General.json")
    Call<Listings> getProductList(@Header("Authorization") String authorization, @Query("category") String category, @Query("rows") int row);

    @GET("v1/Search/General.json")
    Observable<Listings> getProductListRx(@Header("Authorization") String authorization, @Query("category") String category, @Query("rows") int row);
}
