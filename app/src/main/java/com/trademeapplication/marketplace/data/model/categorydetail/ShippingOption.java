
package com.trademeapplication.marketplace.data.model.categorydetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingOption {

    @SerializedName("Type")
    @Expose
    private Integer type;

    @SerializedName("Price")
    @Expose
    private Integer price;

    @SerializedName("Method")
    @Expose
    private String  method;

    @SerializedName("ShippingId")
    @Expose
    private Integer shippingId;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Integer getShippingId() {
        return shippingId;
    }

    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

}
