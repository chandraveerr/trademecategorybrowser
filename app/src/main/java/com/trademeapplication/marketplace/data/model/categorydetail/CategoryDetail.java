
package com.trademeapplication.marketplace.data.model.categorydetail;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryDetail {

    @SerializedName("ListingId")
    @Expose
    private Integer     listingId;

    @SerializedName("Title")
    @Expose
    private String      title;

    @SerializedName("Category")
    @Expose
    private String      category;

    @SerializedName("StartPrice")
    @Expose
    private Double      startPrice;

    @SerializedName("BuyNowPrice")
    @Expose
    private Double      buyNowPrice;

    @SerializedName("StartDate")
    @Expose
    private String      startDate;

    @SerializedName("EndDate")
    @Expose
    private String      endDate;

    // @SerializedName("ListingLength")
    // @Expose
    // private Object listingLength;

    @SerializedName("IsFeatured")
    @Expose
    private Boolean     isFeatured;

    @SerializedName("HasGallery")
    @Expose
    private Boolean     hasGallery;

    @SerializedName("IsBold")
    @Expose
    private Boolean     isBold;

    @SerializedName("AsAt")
    @Expose
    private String      asAt;

    @SerializedName("CategoryPath")
    @Expose
    private String      categoryPath;

    @SerializedName("PhotoId")
    @Expose
    private Integer     photoId;

    @SerializedName("IsNew")
    @Expose
    private Boolean     isNew;

    @SerializedName("RegionId")
    @Expose
    private Integer     regionId;

    @SerializedName("Region")
    @Expose
    private String      region;

    @SerializedName("SuburbId")
    @Expose
    private Integer     suburbId;

    @SerializedName("Suburb")
    @Expose
    private String      suburb;

    @SerializedName("HasReserve")
    @Expose
    private Boolean     hasReserve;

    @SerializedName("HasBuyNow")
    @Expose
    private Boolean     hasBuyNow;

    @SerializedName("NoteDate")
    @Expose
    private String      noteDate;

    @SerializedName("CategoryName")
    @Expose
    private String      categoryName;

    @SerializedName("ReserveState")
    @Expose
    private Integer     reserveState;

    // @SerializedName("Attributes")
    // @Expose
    // private List<Object> attributes = null;
    // @SerializedName("OpenHomes")
    // @Expose
    // private List<Object> openHomes = null;
    @SerializedName("Subtitle")
    @Expose
    private String      subtitle;

    @SerializedName("MinimumNextBidAmount")
    @Expose
    private Double      minimumNextBidAmount;

    @SerializedName("PriceDisplay")
    @Expose
    private String      priceDisplay;

    // @SerializedName("AdditionalData")
    // @Expose
    // private AdditionalData additionalData;
    // @SerializedName("Member")
    // @Expose
    // private Member member;

    @SerializedName("Body")
    @Expose
    private String      body;

    // @SerializedName("Questions")
    // @Expose
    // private Questions questions;

    @SerializedName("Photos")
    @Expose
    private List<Photo> photos = null;

    @SerializedName("AllowsPickups")
    @Expose
    private Integer     allowsPickups;

    // @SerializedName("ShippingOptions")
    // @Expose
    // private List<ShippingOption> shippingOptions = null;
    @SerializedName("PaymentOptions")
    @Expose
    private String      paymentOptions;

    // @SerializedName("DonationRecipient")
    // @Expose
    // private DonationRecipient donationRecipient;
    @SerializedName("IsInTradeProtected")
    @Expose
    private Boolean     isInTradeProtected;

    @SerializedName("CanAddToCart")
    @Expose
    private Boolean     canAddToCart;

    // @SerializedName("EmbeddedContent")
    // @Expose
    // private EmbeddedContent embeddedContent;
    @SerializedName("SupportsQuestionsAndAnswers")
    @Expose
    private Boolean     supportsQuestionsAndAnswers;
    // @SerializedName("PaymentMethods")
    // @Expose
    // private List<PaymentMethod> paymentMethods = null;

    public Integer getListingId() {
        return listingId;
    }

    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Double startPrice) {
        this.startPrice = startPrice;
    }

    public Double getBuyNowPrice() {
        return buyNowPrice;
    }

    public void setBuyNowPrice(Double buyNowPrice) {
        this.buyNowPrice = buyNowPrice;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    // public Object getListingLength() {
    // return listingLength;
    // }
    //
    // public void setListingLength(Object listingLength) {
    // this.listingLength = listingLength;
    // }

    public Boolean getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    public Boolean getHasGallery() {
        return hasGallery;
    }

    public void setHasGallery(Boolean hasGallery) {
        this.hasGallery = hasGallery;
    }

    public Boolean getIsBold() {
        return isBold;
    }

    public void setIsBold(Boolean isBold) {
        this.isBold = isBold;
    }

    public String getAsAt() {
        return asAt;
    }

    public void setAsAt(String asAt) {
        this.asAt = asAt;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getSuburbId() {
        return suburbId;
    }

    public void setSuburbId(Integer suburbId) {
        this.suburbId = suburbId;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public Boolean getHasReserve() {
        return hasReserve;
    }

    public void setHasReserve(Boolean hasReserve) {
        this.hasReserve = hasReserve;
    }

    public Boolean getHasBuyNow() {
        return hasBuyNow;
    }

    public void setHasBuyNow(Boolean hasBuyNow) {
        this.hasBuyNow = hasBuyNow;
    }

    public String getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(String noteDate) {
        this.noteDate = noteDate;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getReserveState() {
        return reserveState;
    }

    public void setReserveState(Integer reserveState) {
        this.reserveState = reserveState;
    }

    // public List<Object> getAttributes() {
    // return attributes;
    // }
    //
    // public void setAttributes(List<Object> attributes) {
    // this.attributes = attributes;
    // }
    //
    // public List<Object> getOpenHomes() {
    // return openHomes;
    // }
    //
    // public void setOpenHomes(List<Object> openHomes) {
    // this.openHomes = openHomes;
    // }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Double getMinimumNextBidAmount() {
        return minimumNextBidAmount;
    }

    public void setMinimumNextBidAmount(Double minimumNextBidAmount) {
        this.minimumNextBidAmount = minimumNextBidAmount;
    }

    public String getPriceDisplay() {
        return priceDisplay;
    }

    public void setPriceDisplay(String priceDisplay) {
        this.priceDisplay = priceDisplay;
    }

    // public AdditionalData getAdditionalData() {
    // return additionalData;
    // }
    //
    // public void setAdditionalData(AdditionalData additionalData) {
    // this.additionalData = additionalData;
    // }

    // public Member getMember() {
    // return member;
    // }
    //
    // public void setMember(Member member) {
    // this.member = member;
    // }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    // public Questions getQuestions() {
    // return questions;
    // }
    //
    // public void setQuestions(Questions questions) {
    // this.questions = questions;
    // }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public Integer getAllowsPickups() {
        return allowsPickups;
    }

    public void setAllowsPickups(Integer allowsPickups) {
        this.allowsPickups = allowsPickups;
    }

    // public List<ShippingOption> getShippingOptions() {
    // return shippingOptions;
    // }
    //
    // public void setShippingOptions(List<ShippingOption> shippingOptions) {
    // this.shippingOptions = shippingOptions;
    // }

    public String getPaymentOptions() {
        return paymentOptions;
    }

    public void setPaymentOptions(String paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    // public DonationRecipient getDonationRecipient() {
    // return donationRecipient;
    // }
    //
    // public void setDonationRecipient(DonationRecipient donationRecipient) {
    // this.donationRecipient = donationRecipient;
    // }

    public Boolean getIsInTradeProtected() {
        return isInTradeProtected;
    }

    public void setIsInTradeProtected(Boolean isInTradeProtected) {
        this.isInTradeProtected = isInTradeProtected;
    }

    public Boolean getCanAddToCart() {
        return canAddToCart;
    }

    public void setCanAddToCart(Boolean canAddToCart) {
        this.canAddToCart = canAddToCart;
    }

    // public EmbeddedContent getEmbeddedContent() {
    // return embeddedContent;
    // }
    //
    // public void setEmbeddedContent(EmbeddedContent embeddedContent) {
    // this.embeddedContent = embeddedContent;
    // }

    public Boolean getSupportsQuestionsAndAnswers() {
        return supportsQuestionsAndAnswers;
    }

    public void setSupportsQuestionsAndAnswers(Boolean supportsQuestionsAndAnswers) {
        this.supportsQuestionsAndAnswers = supportsQuestionsAndAnswers;
    }

    // public List<PaymentMethod> getPaymentMethods() {
    // return paymentMethods;
    // }
    //
    // public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
    // this.paymentMethods = paymentMethods;
    // }

}
