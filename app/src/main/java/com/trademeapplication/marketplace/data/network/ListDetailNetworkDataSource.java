package com.trademeapplication.marketplace.data.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.trademeapplication.marketplace.TradeMeApplication;
import com.trademeapplication.marketplace.data.model.categorydetail.CategoryDetail;
import com.trademeapplication.marketplace.utils.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Network source fetching details of a listing
 */
public class ListDetailNetworkDataSource {

    private static ListDetailNetworkDataSource        sInstance;

    private static String                             LOG_TAG = ListDetailNetworkDataSource.class.getSimpleName();

    // For Singleton instantiation
    private static final Object                       LOCK    = new Object();

    private MutableLiveData<Resource<CategoryDetail>> mListingDetail;

    private GetListDetailService                      service;

    private ListDetailNetworkDataSource() {
        // mListingDetail = new MutableLiveData<>();
        service = RetrofitCreator.getRetrofitInstance().create(GetListDetailService.class);
    }

    /**
     * Get the singleton for this class
     */
    public static ListDetailNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new ListDetailNetworkDataSource();
                Log.d(LOG_TAG, "Made new network data source");
            }
        }
        return sInstance;
    }

    public LiveData<Resource<CategoryDetail>> getListingDetail(String id) {
        mListingDetail = new MutableLiveData<>();
        mListingDetail.postValue(Resource.<CategoryDetail> loading(null));
        fetchListingDetail(id);
        return mListingDetail;
    }

    private void fetchListingDetail(String id) {

        service.getDetailRx(Utils.getAuthHeader(TradeMeApplication.getContext()), id).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DefaultObserver<CategoryDetail>() {

            @Override
            public void onNext(CategoryDetail value) {
                mListingDetail.postValue(Resource.success(value));
            }

            @Override
            public void onError(Throwable e) {
                Log.e(LOG_TAG, e.getMessage());
                mListingDetail.postValue(Resource.<CategoryDetail> error(e.getMessage(), null));
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
