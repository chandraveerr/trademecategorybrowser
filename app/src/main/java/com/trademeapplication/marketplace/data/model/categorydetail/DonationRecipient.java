
package com.trademeapplication.marketplace.data.model.categorydetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DonationRecipient {

    @SerializedName("CharityType")
    @Expose
    private Integer charityType;

    @SerializedName("ImageSource")
    @Expose
    private String  imageSource;

    @SerializedName("Description")
    @Expose
    private String  description;

    @SerializedName("Tagline")
    @Expose
    private String  tagline;

    public Integer getCharityType() {
        return charityType;
    }

    public void setCharityType(Integer charityType) {
        this.charityType = charityType;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

}
