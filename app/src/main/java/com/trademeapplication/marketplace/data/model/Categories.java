package com.trademeapplication.marketplace.data.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Categories {

    @SerializedName("Name")
    public String           name;

    @SerializedName("Subcategories")
    public List<Categories> subcategories = null;

    @SerializedName("Number")
    public String           number;

    @SerializedName("IsLeaf")
    public boolean          isLeaf;

    public String getName() {
        return name;
    }

    public List<Categories> getSubcategories() {
        return subcategories;
    }

    public String getNumber() {
        return number;
    }

    public boolean isLeaf() {
        return isLeaf;
    }
}
