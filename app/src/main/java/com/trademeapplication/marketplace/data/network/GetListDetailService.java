package com.trademeapplication.marketplace.data.network;

import com.trademeapplication.marketplace.data.model.categorydetail.CategoryDetail;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Makes a GET call to get Details of a particular Listing.
 * 
 * @param id
 *            Listing id of a listing.
 * @param authorization
 *            Specifies the OAUTH 1.0 authorization header.
 * @return Listing details.
 */

public interface GetListDetailService {

    @GET("v1/Listings/{id}.json?increment_view_count=false&return_member_profile=false")
    Call<CategoryDetail> getDetail(@Header("Authorization") String authorization, @Path("id") String id);

    @GET("v1/Listings/{id}.json?increment_view_count=false&return_member_profile=false")
    Observable<CategoryDetail> getDetailRx(@Header("Authorization") String authorization, @Path("id") String id);
}
