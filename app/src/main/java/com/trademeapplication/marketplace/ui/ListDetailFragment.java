package com.trademeapplication.marketplace.ui;

import com.bumptech.glide.Glide;
import com.trademeapplication.marketplace.R;
import com.trademeapplication.marketplace.TradeMeApplication;
import com.trademeapplication.marketplace.data.model.categorydetail.CategoryDetail;
import com.trademeapplication.marketplace.data.model.categorydetail.Photo;
import com.trademeapplication.marketplace.utils.InjectorUtils;
import com.trademeapplication.marketplace.viewmodel.ListingDetailViewModel;
import com.trademeapplication.marketplace.viewmodel.ListingDetailViewModelFactory;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This Fragment shows details of a particular listing
 */

public class ListDetailFragment extends Fragment {

    public static String           TAG    = ProductListFragment.class.getSimpleName();

    public static final String     KEY_ID = "key_id";

    private ListingDetailViewModel mViewModel;

    @BindView(R.id.tv_detail_title)
    TextView                       mTitle;

    @BindView(R.id.tv_detail_id)
    TextView                       mId;

    @BindView(R.id.tv_detail_category)
    TextView                       mCategory;

    @BindView(R.id.tv_detail_category_path)
    TextView                       mCategoryPath;

    @BindView(R.id.iv_detail)
    ImageView                      mTopImage;

    @BindView(R.id.tv_detail_error)
    TextView                       mError;

    @BindView(R.id.progressBar_detail)
    ProgressBar                    mProgressBar;

    @BindView(R.id.lv_detail)
    LinearLayout                   mDetailLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(KEY_ID)) {
            String id = getArguments().getString(KEY_ID);

            // Get the ViewModel from the factory
            ListingDetailViewModelFactory factory = InjectorUtils.provideListingDetailViewModelFactory(id);
            mViewModel = ViewModelProviders.of(this, factory).get(ListingDetailViewModel.class);

            mViewModel.getDetails(id).observe(this, categoryDetailResource -> {
                switch (categoryDetailResource.status) {
                case ERROR:
                    mProgressBar.setVisibility(View.GONE);
                    mError.setVisibility(View.VISIBLE);
                    mDetailLayout.setVisibility(View.GONE);
                    break;
                case LOADING:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mError.setVisibility(View.GONE);
                    mDetailLayout.setVisibility(View.GONE);
                    break;
                case SUCCESS:
                    mError.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.GONE);
                    mDetailLayout.setVisibility(View.VISIBLE);
                    CategoryDetail detail = categoryDetailResource.data;
                    if (detail != null) {
                        mCategory.setText(detail.getCategoryName());
                        mCategoryPath.setText(detail.getCategoryPath());
                        mTitle.setText(detail.getTitle());
                        mId.setText(String.valueOf(detail.getListingId()));
                        for (Photo photo : detail.getPhotos()) {
                            if (photo.getKey().equals(detail.getPhotoId())) {
                                Glide.with(TradeMeApplication.getContext()).load(photo.getValue().getGallery()).into(mTopImage);
                            }
                        }
                    }
                    break;
                }

            });
        }
    }
}
