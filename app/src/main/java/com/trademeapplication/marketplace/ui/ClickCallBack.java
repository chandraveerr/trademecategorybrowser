package com.trademeapplication.marketplace.ui;

import android.support.annotation.NonNull;

/**
 * Callback to handle item clicks of category lists
 */

public interface ClickCallBack<T> {

    void onClick(@NonNull T category);
}
