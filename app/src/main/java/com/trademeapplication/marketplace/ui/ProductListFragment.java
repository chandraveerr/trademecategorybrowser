package com.trademeapplication.marketplace.ui;

import com.trademeapplication.marketplace.R;
import com.trademeapplication.marketplace.data.model.Listing;
import com.trademeapplication.marketplace.utils.InjectorUtils;
import com.trademeapplication.marketplace.utils.NavigationUtils;
import com.trademeapplication.marketplace.utils.Utils;
import com.trademeapplication.marketplace.viewmodel.ListingViewModelFactory;
import com.trademeapplication.marketplace.viewmodel.ListingsViewModel;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This Fragment representing a list of Listings available for a subcategory.
 * This Fragment has different presentations for handset and tablet-size
 * devices. On handsets, the fragment presents a list of listings, which when
 * touched, lead to a {@link ListDetailFragment} representing listing details.
 * On tablets, the activity presents the list of listings and details
 * side-by-side using two vertical panes.
 */

public class ProductListFragment extends Fragment {

    public static String       TAG = ProductListFragment.class.getSimpleName();

    private ListingsViewModel  mViewModel;

    private ProductListAdapter mAdapter;

    @BindView(R.id.recycle_view)
    RecyclerView               mRecycleView;

    @BindView(R.id.tv_detail_error)
    TextView                   mError;

    @BindView(R.id.progressBar_detail)
    ProgressBar                mProgressBar;

    private boolean            mTwoPane;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_list, container, false);
        if (rootView.findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupRecycleView();
    }

    private void setupRecycleView() {
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecycleView.setHasFixedSize(true);
        mAdapter = new ProductListAdapter(mCategoryClickCallBack);
        mRecycleView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Utils.ApiConstants.KEY_ID)) {
            String number = getArguments().getString(Utils.ApiConstants.KEY_ID);

            // Get the ViewModel from the factory
            ListingViewModelFactory factory = InjectorUtils.provideListingViewModelFactory(number);
            mViewModel = ViewModelProviders.of(this, factory).get(ListingsViewModel.class);
            mViewModel.getListings(number).observe(this, listingsResource -> {
                switch (listingsResource.status) {
                case SUCCESS:
                    if (listingsResource.data != null && listingsResource.data.getList() != null) {
                        if (listingsResource.data.getList().size() == 0) {
                            mError.setText(R.string.no_listing_text);
                            mError.setVisibility(View.VISIBLE);
                        } else {
                            mError.setVisibility(View.GONE);
                        }
                        mProgressBar.setVisibility(View.GONE);
                        mAdapter.setData(listingsResource.data.getList());
                        mAdapter.notifyDataSetChanged();
                    }
                    break;
                case LOADING:
                    mProgressBar.setVisibility(View.VISIBLE);
                    mError.setVisibility(View.GONE);
                    break;
                case ERROR:
                    mProgressBar.setVisibility(View.GONE);
                    mError.setText(R.string.error_string);
                    mError.setVisibility(View.VISIBLE);
                    break;
                }
            });

        }
    }

    private final ClickCallBack mCategoryClickCallBack = (ClickCallBack<Listing>) listing -> {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            if (mTwoPane) {
                NavigationUtils.startScreenDetailMode(getFragmentManager(), Utils.getDetailFragment(String.valueOf(listing.getListingId())));
            } else {
                NavigationUtils.startScreen(getFragmentManager(), Utils.getDetailFragment(String.valueOf(listing.getListingId())));
            }
        }
    };

}
