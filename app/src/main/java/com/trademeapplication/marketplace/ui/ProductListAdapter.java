package com.trademeapplication.marketplace.ui;

import java.util.ArrayList;
import java.util.List;

import com.bumptech.glide.Glide;
import com.trademeapplication.marketplace.R;
import com.trademeapplication.marketplace.TradeMeApplication;
import com.trademeapplication.marketplace.data.model.Listing;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private List<Listing> mProductList;

    private ClickCallBack mClickCallBack;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
        @BindView(R.id.tv_list_title)
        public TextView  mTitle;

        @BindView(R.id.tv_listing_id)
        public TextView  mId;

        @BindView(R.id.iv_listing_thumb)
        public ImageView mListThumb;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductListAdapter(ClickCallBack clickCallBack) {
        mProductList = new ArrayList<>();
        mClickCallBack = clickCallBack;
    }

    public void setData(List<Listing> data) {
        mProductList = data;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listing_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTitle.setText(mProductList.get(position).getTitle());
        holder.mId.setText(String.valueOf(mProductList.get(position).getListingId()));
        Glide.with(TradeMeApplication.getContext()).load(mProductList.get(position).getPictureHref()).into(holder.mListThumb);
        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mClickCallBack.onClick(mProductList.get(position));
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}
