package com.trademeapplication.marketplace.ui;

import com.trademeapplication.marketplace.R;
import com.trademeapplication.marketplace.data.model.Categories;
import com.trademeapplication.marketplace.utils.InjectorUtils;
import com.trademeapplication.marketplace.utils.NavigationUtils;
import com.trademeapplication.marketplace.utils.Utils;
import com.trademeapplication.marketplace.viewmodel.CategoryViewModel;
import com.trademeapplication.marketplace.viewmodel.CategoryViewModelFactory;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This Fragment shows list of a categories and when clicked upon shows
 * subcategories till leaf is encountered, which when touched lead to a
 * {@link ProductListFragment} representing listing of that particular sub
 * category
 */

public class CategoryListFragment extends Fragment {

    public static String        TAG = CategoryListFragment.class.getSimpleName();

    private CategoryViewModel   mViewModel;

    @BindView(R.id.recycle_view)
    RecyclerView                mRecycleView;

    @BindView(R.id.tv_detail_error)
    TextView                    mError;

    @BindView(R.id.progressBar_detail)
    ProgressBar                 mProgressBar;

    private CategoryListAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_fragment, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupRecycleView();
    }

    private void setupRecycleView() {
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecycleView.setHasFixedSize(true);
        mAdapter = new CategoryListAdapter(mCategoryClickCallBack);
        mRecycleView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String number = String.valueOf(0);
        if (getArguments() != null) {
            number = getArguments().getString(Utils.ApiConstants.KEY_ID);
        }

        // Get the ViewModel from the factory
        CategoryViewModelFactory factory = InjectorUtils.provideCategoryViewModelFactory(number);
        mViewModel = ViewModelProviders.of(this, factory).get(CategoryViewModel.class);

        mViewModel.getCategories(number).observe(this, categoriesResource -> {
            switch (categoriesResource.status) {
            case SUCCESS:
                mProgressBar.setVisibility(View.GONE);
                mError.setVisibility(View.GONE);
                if (categoriesResource.data != null && categoriesResource.data.subcategories != null) {
                    mAdapter.setData(categoriesResource.data.subcategories);
                    mAdapter.notifyDataSetChanged();
                }
                break;
            case LOADING:
                mProgressBar.setVisibility(View.VISIBLE);
                mError.setVisibility(View.GONE);
                break;
            case ERROR:
                mProgressBar.setVisibility(View.GONE);
                mError.setVisibility(View.VISIBLE);
                break;
            }
        });
    }

    private final ClickCallBack mCategoryClickCallBack = (ClickCallBack<Categories>) category -> {

        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            NavigationUtils.startScreen(getFragmentManager(), category.isLeaf() ? Utils.getProductListFragment(category.getNumber()) : Utils.getCategoryListFragment(category.getNumber()));
        }
    };

}
