package com.trademeapplication.marketplace.ui;

import com.trademeapplication.marketplace.R;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;

/**
 * Container activity for all the fragments
 */
public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.recycle_view)
    RecyclerView mRecycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Add list fragment if this is first creation
        if (savedInstanceState == null) {
            CategoryListFragment fragment = new CategoryListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment, CategoryListFragment.TAG).commit();
        }
    }

}
