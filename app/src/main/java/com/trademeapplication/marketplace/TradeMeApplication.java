package com.trademeapplication.marketplace;

import android.app.Application;
import android.content.Context;

/**
 * Application class
 */

public class TradeMeApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}
