package com.trademeapplication.marketplace.viewmodel;

import com.trademeapplication.marketplace.data.DataRepository;
import com.trademeapplication.marketplace.data.model.Categories;
import com.trademeapplication.marketplace.data.network.Resource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

/**
 * Viewmodel class for the Category and sub category lists. It extends android
 * view model class so that it can be lifecycle aware of lifcycle owner objects
 */

public class CategoryViewModel extends ViewModel {

    private LiveData<Resource<Categories>> mCategoryLiveData;

    private DataRepository                 mRepository;

    private String                         mCategoryNumber;

    public LiveData<Resource<Categories>> getCategories(@NonNull String categorynumber) {
        if (!mCategoryNumber.equals(categorynumber) || mCategoryLiveData == null) {
            mCategoryLiveData = mRepository.getCategories(categorynumber);
        }
        return mCategoryLiveData;
    }

    public CategoryViewModel(@NonNull DataRepository repository, @NonNull String categorynumber) {
        mRepository = repository;
        mCategoryNumber = categorynumber;
    }
}
