package com.trademeapplication.marketplace.viewmodel;

import com.trademeapplication.marketplace.data.DataRepository;
import com.trademeapplication.marketplace.data.model.Listings;
import com.trademeapplication.marketplace.data.network.Resource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

/**
 * Created by chandraveer on 02/05/18.
 */

public class ListingsViewModel extends ViewModel {

    private String                       mId;

    private LiveData<Resource<Listings>> mListingLiveData;

    private DataRepository               mRepository;

    public LiveData<Resource<Listings>> getListings(@NonNull String id) {
        if (!mId.equals(id) || mListingLiveData == null) {
            mListingLiveData = mRepository.getListings(id);
        }
        return mListingLiveData;
    }

    public ListingsViewModel(@NonNull DataRepository repository, @NonNull String id) {
        mRepository = repository;
        mId = id;
    }
}
