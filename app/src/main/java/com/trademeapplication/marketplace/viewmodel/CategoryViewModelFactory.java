package com.trademeapplication.marketplace.viewmodel;

import com.trademeapplication.marketplace.data.DataRepository;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

/**
 * Created by chandraveer on 02/05/18.
 */

public class CategoryViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final DataRepository mRepository;

    private final String         mCategoryNumber;

    public CategoryViewModelFactory(DataRepository repository, String categoryNumber) {
        this.mRepository = repository;
        this.mCategoryNumber = categoryNumber;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        // noinspection unchecked
        return (T) new CategoryViewModel(mRepository, mCategoryNumber);
    }
}
