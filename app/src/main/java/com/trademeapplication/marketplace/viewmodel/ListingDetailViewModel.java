package com.trademeapplication.marketplace.viewmodel;

import com.trademeapplication.marketplace.data.DataRepository;
import com.trademeapplication.marketplace.data.model.categorydetail.CategoryDetail;
import com.trademeapplication.marketplace.data.network.Resource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

/**
 * Created by chandraveer on 03/05/18.
 */

public class ListingDetailViewModel extends ViewModel {

    private String                             mId;

    private LiveData<Resource<CategoryDetail>> mListingLiveData;

    private DataRepository                     mRepository;

    public LiveData<Resource<CategoryDetail>> getDetails(@NonNull String id) {
        if (!mId.equals(id) || mListingLiveData == null) {
            mListingLiveData = mRepository.getListingDetails(id);
        }
        return mListingLiveData;
    }

    public ListingDetailViewModel(@NonNull DataRepository repository, @NonNull String id) {
        mRepository = repository;
        mId = id;
    }
}
