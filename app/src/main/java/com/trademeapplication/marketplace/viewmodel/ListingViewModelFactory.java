package com.trademeapplication.marketplace.viewmodel;

import com.trademeapplication.marketplace.data.DataRepository;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

/**
 * Created by chandraveer on 04/05/18.
 */

public class ListingViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final DataRepository mRepository;

    private final String         mId;

    public ListingViewModelFactory(DataRepository repository, String id) {
        this.mRepository = repository;
        this.mId = id;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        // noinspection unchecked
        return (T) new ListingsViewModel(mRepository, mId);
    }
}
